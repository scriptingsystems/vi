[Fuente]: https://www.redhat.com/sysadmin/use-vim-macros

To record a macro and save it to a register, type the key q followed by a letter from a to z that represents the register to save the macro, followed by all commands you want to record, and then type the key q again to stop the recording.
```
q<register><commands>q
```


# View and load macros

Because macros are saved into Vim registers, you can view saved macros by using the command :reg to see the content of all registers:
```
:reg
--- Registers ---
""   ,
"h   0r#f"xldf,A,^[p0df,$px:s/,/\t/g^Mj
"-   ,
...
```

# Replay macros

To repeat the macro execution, press **@@**. 
Finally, you can repeat the macro on all remaining lines by using Vim **number<COMMAND>** syntax. For example, to execute the macro on the five remaining lines, press **5@@** or **5@h**.
